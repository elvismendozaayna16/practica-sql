--Ejercicios propuestos

Ejercicio 01
--Una empresa registra los datos de sus empleados en una tabla llamada "empleados".

1--Elimine la tabla "empleados":


drop table empleados;



2--Cree la tabla:


create table empleados(
    legajo number(3),
    documento char(8) not null,
    nombre varchar2(30) not null,
    primary key(legajo)
);




3--Elimine la secuencia "sec_legajoempleados" y luego cr�ela estableciendo el valor m�nimo (1), m�ximo (999), valor inicial (100), valor de incremento (2) y no circular. Finalmente inicialice la secuencia.

DROP  sec_legajoempleados;

CREATE SEQUENCE sec_legajoempleados
    MINVALUE 1
    MAXVALUE 999
    START WITH 100
    INCREMENT BY 2
    NOCYCLE;

ALTER SEQUENCE sec_legajoempleados RESTART;


4---Ingrese algunos registros, empleando la secuencia creada para los valores de la clave primaria:



insert into empleados
values (sec_legajoempleados.currval,'22333444','Ana Acosta');

insert into empleados
values (sec_legajoempleados.nextval,'23444555','Betina Bustamante');

insert into empleados
values (sec_legajoempleados.nextval,'24555666','Carlos Caseros');

insert into empleados
values (sec_legajoempleados.nextval,'25666777','Diana Dominguez');

insert into empleados
values (sec_legajoempleados.nextval,'26777888','Estela Esper');



5--Recupere los registros de "libros" para ver los valores de clave primaria.

Note que los valores se incrementaron en 2, porque as� se estableci� el valor de incremento al crear la secuencia.

SELECT * FROM empleados;

6--Vea el valor actual de la secuencia empleando la tabla "dual". Retorna 108.

SELECT sec_legajoempleados.currval FROM dual;

7--Recupere el valor siguiente de la secuencia empleando la tabla "dual" Retorna 110.

SELECT sec_legajoempleados.nextval FROM dual;


8--Ingrese un nuevo empleado (recuerde que la secuencia ya tiene el pr�ximo valor, emplee "currval" para almacenar el valor de legajo)

INSERT INTO empleados (legajo, documento, nombre)
VALUES (sec_legajoempleados.currval, '28999000', 'Fernando Fern�ndez');

9--Recupere los registros de "libros" para ver el valor de clave primaria ingresado anteriormente.
SELECT * FROM libros;


10--Incremente el valor de la secuencia empleando la tabla "dual" (retorna 112)
SELECT sec_legajoempleados.nextval FROM dual;


11--Ingrese un empleado con valor de legajo "112".
INSERT INTO empleados (legajo, documento, nombre)
VALUES (112, '27888999', 'Fernando Fern�ndez');


12--Intente ingresar un registro empleando "currval":



insert into empleados
values (sec_legajoempleados.currval,'29000111','Hector Huerta');


Mensaje de error porque el legajo est� repetido y la clave primaria no puede repetirse.


13--Incremente el valor de la secuencia. Retorna 114.

SELECT sec_legajoempleados.nextval FROM dual;

14--{Ingrese el registro del punto 11.

INSERT INTO empleados (legajo, documento, nombre)
VALUES (11, '28999999', 'Fernando Fern�ndez');

Ahora si lo permite, pues el valor retornado por "currval" no est� repetido en la tabla "empleados".


15--Recupere los registros.
SELECT * FROM empleados;


16--Vea las secuencias existentes y analice la informaci�n retornada.


Debe aparecer "sec_legajoempleados".
SELECT * FROM user_sequences WHERE sequence_name = 'sec_legajoempleados';


17--Vea todos los objetos de la base de datos actual que contengan en su nombre la cadena "EMPLEADOS".

Debe aparacer la tabla "empleados" y la secuencia "sec_legajoempleados".

SELECT object_name, object_type
FROM user_objects
WHERE object_name LIKE '%EMPLEADOS%';

18--Elimine la secuencia creada.

DROP SEQUENCE sec_legajoempleados;

19--Consulte todos los objetos de la base de datos que sean secuencias y verifique que "sec_legajoempleados" ya no existe.
SELECT object_name
FROM user_objects
WHERE object_type = 'SEQUENCE';
