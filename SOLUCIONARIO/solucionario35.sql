--Ejercicios propuestos
Una empresa registra los datos de sus empleados en una tabla llamada "empleados".

1--Elimine la tabla "empleados":


drop table empleados;



2--Cree la tabla:


create table empleados(
    legajo number(3),
    documento char(8) not null,
    nombre varchar2(30) not null,
    primary key(legajo)
);




3--Elimine la secuencia "sec_legajoempleados" y luego cr�ela estableciendo el valor m�nimo (1), m�ximo (210), valor inicial (206), valor de incremento (2) y no circular. Finalmente inicialice la secuencia.

drop sequence sec_legajoempleados;

-- secuencia con los nuevos par�metros
create sequence sec_legajoempleados
  minvalue 1
  maxvalue 210
  start with 206
  increment by 2
  nocycle;

-- Inicializar la secuencia
select sec_legajoempleados.nextval from dual;


4--Ingrese algunos registros, empleando la secuencia creada para los valores de la clave primaria.



insert into empleados
values (sec_legajoempleados.currval,'22333444','Ana Acosta');

insert into empleados
values (sec_legajoempleados.nextval,'23444555','Betina Bustamante');

insert into empleados
values (sec_legajoempleados.nextval,'24555666','Carlos Caseros');




5--Recupere los registros de "libros" para ver los valores de clave primaria.

SELECT * FROM libros;

6---Vea el valor actual de la secuencia empleando la tabla "dual"
SELECT sec_legajoempleados.currval FROM dual;


7--Intente ingresar un registro empleando "nextval":



insert into empleados
values (sec_legajoempleados.nextval,'25666777','Diana Dominguez');


Oracle muestra un mensaje de error indicando que la secuencia ha llegado a su valor m�ximo.


8--Altere la secuencia modificando el atributo "maxvalue" a 999.

ALTER SEQUENCE sec_legajoempleados MAXVALUE 999;
9---Obtenga informaci�n de la secuencia.
SELECT sequence_name, min_value, max_value, increment_by, last_number
FROM user_sequences
WHERE sequence_name = 'SEC_LEGAJOEMPLEADOS';

10--Ingrese el registro del punto 7.

INSERT INTO empleados (legajo, documento, nombre)
VALUES (sec_legajoempleados.nextval, '26777888', 'Estela Esper');
11--Recupere los registros.
SELECT * FROM empleados;

12--Modifique la secuencia para que sus valores se incrementen en 1.
ALTER SEQUENCE sec_legajoempleados INCREMENT BY 1;

13---Ingrese un nuevo registro:



insert into empleados
values (sec_legajoempleados.nextval,'26777888','Federico Fuentes');




14--Recupere los registros.

SELECT * FROM empleados;
15--Elimine la secuencia creada.
DROP SEQUENCE sec_legajoempleados;

16---Consulte todos los objetos de la base de datos que sean secuencias y verifique que "sec_legajoempleados" ya no existe.

SELECT sequence_name
FROM user_sequences;