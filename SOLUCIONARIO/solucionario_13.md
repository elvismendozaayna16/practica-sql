# 13. Vaciar la tabla (truncate table)

## Ejercicio de laboratorio

Trabajamos con la tabla "libros" que almacena los datos de los libros de una librería.

Eliminamos la tabla:

```sql
drop table libros;
```
SALIDA
```sh
```

Creamos la tabla:

```sql
create table libros(
    codigo number(4),
    titulo varchar2(30),
    autor varchar2(20),
    editorial varchar2(15),
    precio number(5,2)
);
```

SALIDA
```sh
```

Agregamos algunos registros:

```sql
insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);
```
SALIDA
```sh
```
Seleccionamos todos los registros:

```sql
select *from libros;
```
SALIDA
```sh
```
Truncamos la tabla:

```sql
truncate table libros;
```
SALIDA
```sh
```
Si consultamos la tabla, vemos que aún existe pero ya no tiene registros:

```sql
select *from libros;
```
SALIDA
```sh
```

Ingresamos nuevamente algunos registros:

```sql
insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);
```
SALIDA
```sh
```
Eliminemos todos los registros con "delete":

```sql
delete from libros;
```
SALIDA
```sh
```
Si consultamos la tabla, vemos que aún existe pero ya no tiene registros:

```sql
select *from libros;
```
SALIDA
```sh
```
Ingresamos nuevamente algunos registros:

```sql
insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);
```
SALIDA
```sh
```

Eliminamos la tabla:

```sql
drop table libros;
```
SALIDA
```sh
```

Intentamos seleccionar todos los registros:

```sql
select *from libros;
```
Aparece un mensaje de error, la tabla no existe.

SALIDA
```sh
```
Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    codigo number(4),
    titulo varchar2(30),
    autor varchar2(20),
    editorial varchar2(15),
    precio number(5,2)
);

insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);

select *from libros;

truncate table libros;

select *from libros;

insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);

delete from libros;

select *from libros;

insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);

drop table libros;

select *from libros;
```
SALIDA
```sh
```
