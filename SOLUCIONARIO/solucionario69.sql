--Ejercicios propuestos
Trabaje con una tabla llamada "empleados" y "secciones".

1--Elimine las tablas y cr�elas:


drop table empleados;
drop table secciones;

create table secciones(
    codigo number(2),
    nombre varchar(20),
    primary key (codigo)
);

create table empleados(
    apellido varchar2(20),
    nombre varchar2(20) not null,
    domicilio varchar2(30),
    seccion number(2),
    sueldo number(8,2),
    constraint CK_empleados_sueldo
    check (sueldo>=0) disable,
    fechaingreso date,
    constraint FK_empleados_seccion
    foreign key (seccion)
    references secciones(codigo)
    on delete set null
 );



2---Ingrese algunos registros en ambas tablas:


insert into secciones values(8,'Secretaria');
insert into secciones values(9,'Contaduria');
insert into secciones values(10,'Sistemas');
insert into empleados values('Lopez','Juan','Colon 123',8,505.50,'10/10/1980');
insert into empleados values('Gonzalez','Juana','Avellaneda 222',9,600,'01/05/1990');
insert into empleados values('Perez','Luis','Caseros 987',10,800,'12/09/2000');




3--Elimine el campo "domicilio" y luego verifique la eliminaci�n
ALTER TABLE empleados DROP COLUMN domicilio;
DESCRIBE empleados;
4--Vea las restricciones de "empleados" (1 restricci�n "foreign key" y 2 "check")
SELECT constraint_name, constraint_type FROM user_constraints WHERE table_name = 'EMPLEADOS';


5--Intente eliminar el campo "codigo" de "secciones"
ALTER TABLE secciones DROP COLUMN codigo;

6--Elimine la restricci�n "foreign key" de "empleados", luego elimine el campo "codigo" de "secciones" y verifique la eliminaci�n
ALTER TABLE empleados DROP CONSTRAINT FK_empleados_seccion;
ALTER TABLE secciones DROP COLUMN codigo;
DESCRIBE secciones;

7--Verifique que al eliminar el campo "codigo" de "secciones" se ha eliminado la "primary key" de "secciones"

SELECT CONSTRAINT_NAME, CONSTRAINT_TYPE
FROM USER_CONSTRAINTS
WHERE TABLE_NAME = 'SECCIONES';

8--Elimine el campo "sueldo" y verifique que la restricci�n sobre tal campo se ha eliminado

SELECT CONSTRAINT_NAME, CONSTRAINT_TYPE
FROM USER_CONSTRAINTS
WHERE TABLE_NAME = 'EMPLEADOS';

9--Cree un �ndice no �nico por el campo "apellido" y verifique su existencia consultando "user_indexes"
CREATE INDEX idx_apellido ON empleados(apellido);

SELECT INDEX_NAME, TABLE_NAME, COLUMN_NAME, UNIQUENESS
FROM USER_INDEXES
WHERE TABLE_NAME = 'EMPLEADOS' AND COLUMN_NAME = 'APELLIDO';

10--Elimine el campo "apellido" y verifique que el �ndice se ha eliminado

ALTER TABLE empleados DROP COLUMN apellido;
SELECT INDEX_NAME, TABLE_NAME, COLUMN_NAME
FROM USER_INDEXES
WHERE TABLE_NAME = 'EMPLEADOS' AND COLUMN_NAME = 'APELLIDO';


11--Elimine 2 campos de "empleados" y vea la estructura de la tabla

ALTER TABLE empleados DROP COLUMN fechaingreso;
ALTER TABLE empleados DROP COLUMN seccion;

DESCRIBE empleados;

12--Intente eliminar el �nico campo de "empleados"
ALTER TABLE empleados DROP COLUMN nombre;
