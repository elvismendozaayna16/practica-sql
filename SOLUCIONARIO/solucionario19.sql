---Ejercicios propuestos
--Un comercio que vende art�culos de computaci�n registra los datos de sus art�culos en una tabla con ese nombre.

--1Elimine la tabla:


drop table articulos;



2--Cree la tabla:


create table articulos(
    codigo number(4),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(8,2),
    cantidad number(3) default 0,
    primary key (codigo)
);



3--Ingrese algunos registros:


insert into articulos
values (101,'impresora','Epson Stylus C45',400.80,20);

insert into articulos
values (203,'impresora','Epson Stylus C85',500,30);

insert into articulos
values (205,'monitor','Samsung 14',800,10);

insert into articulos
values (300,'teclado','ingles Biswal',100,50);




--4El comercio hace un descuento del 15% en ventas mayoristas. Necesitamos recuperar el c�digo, nombre, decripci�n de todos los art�culos con una columna extra que muestre el precio de cada art�culo para la venta mayorista con el siguiente encabezado "precio mayorista"

SELECT codigo, nombre, descripcion, precio * 0.85 AS "precio mayorista"
FROM articulos;


--5Muestre los precios de todos los art�culos, concatenando el nombre y la descripci�n con el encabezado "art�culo" (sin emplear "as" ni comillas)

SELECT CONCAT(nombre, ' ', descripcion) art�culo, precio
FROM articulos;


---6Muestre todos los campos de los art�culos y un campo extra, con el encabezado "monto total" en la que calcule el monto total en dinero de cada art�culo (precio por cantidad)

SELECT *, (precio * cantidad) monto_total
FROM articulos;


--7Muestre la descripci�n de todas las impresoras junto al precio con un 20% de recargo con un encabezado que lo especifique.

SELECT descripcion, precio * 1.2 AS "precio con recargo"
FROM articulos
WHERE nombre = 'impresora';
