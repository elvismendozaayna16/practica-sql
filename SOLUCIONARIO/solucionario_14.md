# 14. Tipos de datos alfanuméricos

## Ejercicios propuestos

## Ejercico 01

Una concesionaria de autos vende autos usados y almacena los datos de los autos en una tabla llamada "autos".

1. Elimine la tabla "autos"
```sql

```
SALIDA
```sh
```

2. Cree la tabla eligiendo el tipo de dato adecuado para cada campo, estableciendo el campo "patente" como clave primaria:

```sql
create table autos(
    patente char(6),
    marca varchar2(20),
    modelo char(4),
    precio number(8,2),
    primary key (patente)
);
```

Hemos definido el campo "patente" de tipo "char" y no "varchar2" porque la cadena de caracteres siempre tendrá la misma longitud (6 caracteres). Lo mismo sucede con el campo "modelo", en el cual almacenaremos el año, necesitamos 4 caracteres fijos.
SALIDA
```sh
```

3. Ingrese los siguientes registros:

```sql
insert into autos (patente,marca,modelo,precio) values('ABC123','Fiat 128','1970',15000);
insert into autos (patente,marca,modelo,precio) values('BCD456','Renault 11','1990',40000);
insert into autos (patente,marca,modelo,precio) values('CDE789','Peugeot 505','1990',80000);
insert into autos (patente,marca,modelo,precio) values('DEF012','Renault Megane','1998',95000);
```
SALIDA
```sh
```

4. Ingrese un registro omitiendo las comillas en el valor de "modelo"
Oracle convierte el valor a cadena.
```sql

```
SALIDA
```sh
```
5. Vea cómo se almacenó.
```sql

```
SALIDA
```sh
```
6. Seleccione todos los autos modelo "1990"
```sql

```
SALIDA
```sh
```
7. Intente ingresar un registro con un valor de patente de 7 caracteres
```sql

```
SALIDA
```sh
```
8. Intente ingresar un registro con valor de patente repetida.
```sql

```
SALIDA
```sh
```
## Ejercico 02

Una empresa almacena los datos de sus clientes en una tabla llamada "clientes".

1. Elimine la tabla "clientes"
```sql

```
SALIDA
```sh
```

2. Créela eligiendo el tipo de dato más adecuado para cada campo:

```sql
create table clientes(
    documento char(8) not null,
    apellido varchar2(20),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2 (11)
);
```
SALIDA
```sh
```
3. Analice la definición de los campos. Se utiliza char(8) para el documento porque siempre constará de 8 caracteres. Para el número telefónico se usar "varchar2" y no un tipo numérico porque si bien es un número, con él no se realizarán operaciones matemáticas.

```sql

```
SALIDA
```sh
```
4. Ingrese algunos registros:

```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('22333444','Perez','Juan','Sarmiento 980','4223344');
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('23444555','Perez','Ana','Colon 234',null);
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('30444555','Garcia','Luciana','Caseros 634',null);
```
SALIDA
```sh
```
5. Intente ingresar un registro con más caracteres que los permitidos para el campo "telefono"
```sql

```
SALIDA
```sh
```
6. Intente ingresar un registro con más caracteres que los permitidos para el campo "documento"
```sql

```
SALIDA
```sh
```
7. Intente ingresar un registro omitiendo las comillas en el campo "apellido"
```sql

```
SALIDA
```sh
```
8. Seleccione todos los clientes de apellido "Perez" (2 registros)
```sql

```
SALIDA
```sh
```