# 15. Tipos de datos numéricos

## Ejercicios propuestos

## Ejercicio 01

Un banco tiene registrados las cuentas corrientes de sus clientes en una tabla llamada "cuentas".
La tabla contiene estos datos:

```sh
 Número de Cuenta        Documento       Nombre          Saldo
 ______________________________________________________________
 1234                         25666777        Pedro Perez     500000.60
 2234                         27888999        Juan Lopez      -250000
 3344                         27888999        Juan Lopez      4000.50
 3346                         32111222        Susana Molina   1000
```

1. Elimine la tabla "cuentas":

```sql
drop table cuentas;
```
SALIDA
```sh
```
2. Cree la tabla eligiendo el tipo de dato adecuado para almacenar los datos descriptos arriba:


* Número de cuenta: entero hasta 9999, no nulo, no puede haber valores repetidos, clave primaria;

* Documento del propietario de la cuenta: cadena de caracteres de 8 de longitud (siempre 8), no nulo;

* Nombre del propietario de la cuenta: cadena de caracteres de 30 de longitud,

* Saldo de la cuenta: valores que no superan 999999.99

```sql
create table cuentas(
    numero number(4) not null,
    documento char(8),
    nombre varchar2(30),
    saldo number(8,2),
    primary key (numero)
);
```

SALIDA
```sh
```
3. Ingrese los siguientes registros:

```sql
insert into cuentas(numero,documento,nombre,saldo) values('1234','25666777','Pedro Perez',500000.60);
insert into cuentas(numero,documento,nombre,saldo) values('2234','27888999','Juan Lopez',-250000);
insert into cuentas(numero,documento,nombre,saldo) values('3344','27888999','Juan Lopez',4000.50);
insert into cuentas(numero,documento,nombre,saldo) values('3346','32111222','Susana Molina',1000);
```

Note que hay dos cuentas, con distinto número de cuenta, de la misma persona.

SALIDA
```sh
```
4. Seleccione todos los registros cuyo saldo sea mayor a "4000" (2 registros)
```sql

```
SALIDA
```sh
```
5. Muestre el número de cuenta y saldo de todas las cuentas cuyo propietario sea "Juan Lopez" (2 registros)
```sql

```
SALIDA
```sh
```
6. Muestre las cuentas con saldo negativo (1 registro)
```sql

```
SALIDA
```sh
```
7. Muestre todas las cuentas cuyo número es igual o mayor a "3000" (2 registros)
```sql

```
SALIDA
```sh
```
## Ejercicio 02

Una empresa almacena los datos de sus empleados en una tabla "empleados" que guarda los siguientes datos: nombre, documento, sexo, domicilio, sueldobasico.

1. Elimine la tabla:

```sql
drop table empleados;
```
SALIDA
```sh
```
2. Cree la tabla eligiendo el tipo de dato adecuado para cada campo:

```sql
create table empleados(
    nombre varchar2(30),
    documento char(8),
    sexo char(1),
    domicilio varchar2(30),
    sueldobasico number(7,2),--máximo estimado 99999.99
    cantidadhijos number(2)--no superará los 99
);
```
SALIDA
```sh
```

3. Ingrese algunos registros:

```sql
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Juan Perez','22333444','m','Sarmiento 123',500,2);
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Ana Acosta','24555666','f','Colon 134',850,0);
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Bartolome Barrios','27888999','m','Urquiza 479',10000.80,4);
```
SALIDA
```sh
```
4. Ingrese un valor de "sueldobasico" con más decimales que los definidos (redondea los decimales al valor más cercano 800.89)
```sql

```
SALIDA
```sh
```
5. Intente ingresar un sueldo que supere los 7 dígitos (no lo permite)

```sql

```
SALIDA
```sh
```
6. Muestre todos los empleados cuyo sueldo no supere los 900 pesos
```sql

```
SALIDA
```sh
```
7. Seleccione los nombres de los empleados que tengan hijos (3 registros)
```sql

```
SALIDA
```sh
```