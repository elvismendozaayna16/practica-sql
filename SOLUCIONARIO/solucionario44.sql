Ejercicios  de laboratorio
Una playa de estacionamiento almacena cada d�a los datos de los veh�culos que ingresan en la tabla llamada "vehiculos".

1--Setee el formato de "date" para que nos muestre hora y minutos:


alter SESSION SET NLS_DATE_FORMAT = 'HH24:MI';



2--Elimine la tabla y cr�ela con la siguiente estructura:


drop table vehiculos;

create table vehiculos(
    patente char(6) not null,
    tipo char(1),--'a'=auto, 'm'=moto
    horallegada date not null,
    horasalida date
);



3--Establezca una restricci�n "check" que admita solamente los valores "a" y "m" para el campo "tipo":


alter table vehiculos
add constraint CK_vehiculos_tipo
check (tipo in ('a','m'));




4---Agregue una restricci�n "primary key" que incluya los campos "patente" y "horallegada"

ALTER TABLE vehiculos
ADD CONSTRAINT PK_vehiculos PRIMARY KEY (patente, horallegada);

5--Ingrese un veh�culo.
INSERT INTO vehiculos (patente, tipo, horallegada, horasalida)
VALUES ('ABC123', 'a', TO_DATE('01/06/2023 09:30', 'DD/MM/YYYY HH24:MI'), NULL);


6--Intente ingresar un registro repitiendo la clave primaria.

INSERT INTO vehiculos (patente, tipo, horallegada, horasalida)
VALUES ('ABC123', 'm', TO_DATE('01/06/2023 10:00', 'DD/MM/YYYY HH24:MI'), NULL);

7--Ingrese un registro repitiendo la patente pero no la hora de llegada.

INSERT INTO vehiculos (patente, tipo, horallegada, horasalida)
VALUES ('ABC123', 'm', TO_DATE('02/06/2023 11:00', 'DD/MM/YYYY HH24:MI'), NULL);

8--Ingrese un registro repitiendo la hora de llegada pero no la patente.
INSERT INTO vehiculos (patente, tipo, horallegada, horasalida)
VALUES ('DEF456', 'm', TO_DATE('02/06/2023 09:30', 'DD/MM/YYYY HH24:MI'), NULL);


9---Vea todas las restricciones para la tabla "vehiculos"
--aparecen 4 filas, 3 correspondientes a restricciones "check" y 1 a "primary key". Dos de las restricciones de control tienen nombres dados por Oracle.

SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'VEHICULOS';

10--Elimine la restricci�n "primary key"

ALTER TABLE vehiculos
DROP CONSTRAINT nombre_restriccion;

11--Vea si se ha eliminado.Ahora aparecen 3 restricciones.

SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'vehiculos';

12--Elimine la restricci�n de control que establece que el campo "patente" no sea nulo (busque el nombre consultando "user_constraints").

ALTER TABLE vehiculos
DROP CONSTRAINT <nombre_restriccion>;
SELECT constraint_name
FROM user_constraints
WHERE table_name = 'vehiculos' AND constraint_type = 'C';

13--Vea si se han eliminado.

SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'VEHICULOS';

14--Vuelva a establecer la restricci�n "primary key" eliminada.

ALTER TABLE vehiculos
ADD CONSTRAINT PK_vehiculos PRIMARY KEY (patente, horallegada);

15--La playa quiere incluir, para el campo "tipo", adem�s de los valores permitidos "a" (auto) y "m" (moto), el caracter "c" (cami�n). No puede modificar la restricci�n, debe eliminarla y luego redefinirla con los 3 valores.
ALTER TABLE vehiculos
DROP CONSTRAINT CK_vehiculos_tipo;


ALTER TABLE vehiculos
ADD CONSTRAINT CK_vehiculos_tipo
CHECK (tipo IN ('a', 'm', 'c'));


16--Consulte "user_constraints" para ver si la condici�n de chequeo de la restricci�n "CK_vehiculos_tipo" se ha modificado.
SELECT constraint_name, search_condition
FROM user_constraints
WHERE table_name = 'VEHICULOS';
