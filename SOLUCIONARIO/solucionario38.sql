---Ejercicios propuestos
Una empresa de remises tiene registrada la información de sus vehículos en una tabla llamada "remis".

1--Elimine la tabla:


drop table remis;



2---Cree la tabla con la siguiente estructura:


create table remis(
    numero number(5),
    patente char(6),
    marca varchar2(15),
    modelo char(4)
);




3--Ingrese algunos registros, 2 de ellos con patente repetida y alguno con patente nula.
INSERT INTO remis(numero, patente, marca, modelo) VALUES (1001, 'ABC123', 'Ford', '2020');
INSERT INTO remis(numero, patente, marca, modelo) VALUES (1002, 'XYZ789', 'Chevrolet', '2018');
INSERT INTO remis(numero, patente, marca, modelo) VALUES (1003, 'ABC123', 'Fiat', '2019');
INSERT INTO remis(numero, patente, marca, modelo) VALUES (1004, NULL, 'Volkswagen', '2017');


4--Agregue una restricción "primary key" para el campo "numero".
ALTER TABLE remis ADD CONSTRAINT pk_numero PRIMARY KEY (numero);


5--Intente agregar una restricción "unique" para asegurarse que la patente del remis no tomará valores repetidos. No se puede porque hay valores duplicados, un mensaje indica que se encontraron claves duplicadas.

ALTER TABLE remis ADD CONSTRAINT uk_patente UNIQUE (patente);

6--Elimine el registro con patente duplicada y establezca la restricción. Note que hay 1 registro con valor nulo en "patente".

DELETE FROM remis WHERE ROWID IN (
    SELECT rid FROM (
        SELECT ROWID rid, ROW_NUMBER() OVER (PARTITION BY patente ORDER BY ROWID) rn FROM remis
    ) WHERE rn > 1
);

ALTER TABLE remis ADD CONSTRAINT uk_patente UNIQUE (patente);

7--Intente ingresar un registro con patente repetida (no lo permite)

INSERT INTO remis(numero, patente, marca, modelo) VALUES (1005, 'ABC123', 'Toyota', '2015');

8--Ingrese un registro con valor nulo para el campo "patente".

INSERT INTO remis(numero, patente, marca, modelo) VALUES (1006, NULL, 'Renault', '2016');

9--Muestre la información de las restricciones consultando "user_constraints" y "user_cons_columns" y analice la información retornada (2 filas en cada consulta)
SELECT constraint_name, constraint_type, table_name FROM user_constraints WHERE table_name = 'REMIS';

SELECT constraint_name, column_name FROM user_cons_columns WHERE table_name = 'REMIS';
