Una empresa almacena la informaci�n de sus clientes en una tabla llamada "clientes".

1- Elimine la tabla:

 drop table clientes;
2- Cree la tabla:

 create table clientes(
  nombre varchar2(40),
  documento char(8),
  domicilio varchar2(30),
  ciudad varchar2(30)
 );
3- Ingrese algunos registros:

 insert into clientes values('Juan Perez','22222222','Colon 1123','Cordoba');
 insert into clientes values('Karina Lopez','23333333','San Martin 254','Cordoba');
 insert into clientes values('Luis Garcia','24444444','Caseros 345','Cordoba');
 insert into clientes values('Marcos Gonzalez','25555555','Sucre 458','Santa Fe');
 insert into clientes values('Nora Torres','26666666','Bulnes 567','Santa Fe');
 insert into clientes values('Oscar Luque','27777777','San Martin 786','Santa Fe');
 insert into clientes values('Pedro Perez','28888888','Colon 234','Buenos Aires');
 insert into clientes values('Rosa Rodriguez','29999999','Avellaneda 23','Buenos Aires');
4- Cree o reemplace la vista "vista_clientes" para que recupere el nombre y ciudad de todos los clientes que no sean de "Cordoba" sin emplear "with check option"
 create or replace view vista_clientes
 as
  select nombre, ciudad
  from clientes
  where ciudad <>'Cordoba';

5- Cree o reemplace la vista "vista_clientes2" para que recupere el nombre y ciudad de todos los clientes que no sean de "Cordoba" empleando "with check option"
create or replace view vista_clientes2
 as
  select nombre, ciudad
  from clientes
  where ciudad <>'Cordoba'
  with check option;

6- Consulte ambas vistas
 select *from vista_clientes;
 select *from vista_clientes2;

7- Intente modificar la ciudad del cliente "Pedro Perez" a "Cordoba" trav�s de la vista que est� restringida.
 update vista_clientes2 set ciudad='Cordoba' where nombre='Pedro Perez';


8- Realice la misma modificaci�n que intent� en el punto anterior a trav�s de la vista que no est� restringida
 update vista_clientes set ciudad='Cordoba' where nombre='Pedro Perez';


9- Actualice la ciudad del cliente "Oscar Luque" a "Buenos Aires" mediante la vista restringida
 update vista_clientes2 set ciudad='Buenos Aires' where nombre='Oscar Luque';


10- Verifique que "Oscar Luque" a�n se incluye en la vista
 select *from vista_clientes2;


11- Intente ingresar un empleado de "Cordoba" en la vista restringida
 insert into vista_clientes2 values('Viviana Valdez','Cordoba');


12- Ingrese el empleado anterior a trav�s de la vista no restringida
 insert into vista_clientes values('Viviana Valdez','Cordoba');


13- Ingrese un empleado de "Salta" en la vista restringida
 insert into vista_clientes2 values('Viviana Valdez','Salta');


14- Verifique que el nuevo registro est� incluido en la vista
 select *from vista_clientes2;

