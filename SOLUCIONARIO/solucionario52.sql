Ejercicios propuestos
Una agencia matrimonial almacena la informaci�n de sus clientes en una tabla llamada "clientes".

1--Elimine la tabla y cr�ela:


drop table clientes;

create table clientes(
    nombre varchar2(30),
    sexo char(1),--'f'=femenino, 'm'=masculino
    edad number(2),
    domicilio varchar2(30)
);



2--Ingrese los siguientes registros:


insert into clientes values('Maria Lopez','f',45,'Colon 123');
insert into clientes values('Liliana Garcia','f',35,'Sucre 456');
insert into clientes values('Susana Lopez','f',41,'Avellaneda 98');
insert into clientes values('Juan Torres','m',44,'Sarmiento 755');
insert into clientes values('Marcelo Oliva','m',56,'San Martin 874');
insert into clientes values('Federico Pereyra','m',38,'Colon 234');
insert into clientes values('Juan Garcia','m',50,'Peru 333');




3--La agencia necesita la combinaci�n de todas las personas de sexo femenino con las de sexo masculino. Use un "cross join" (12 filas)
SELECT f.nombre, f.sexo, f.edad, f.domicilio, m.nombre, m.sexo, m.edad, m.domicilio
FROM clientes f
CROSS JOIN clientes m
WHERE f.sexo = 'f' AND m.sexo = 'm';


4--Obtenga la misma salida anterior pero realizando un "join"

SELECT f.nombre, f.sexo, f.edad, f.domicilio, m.nombre, m.sexo, m.edad, m.domicilio
FROM clientes f
JOIN clientes m ON f.sexo = 'f' AND m.sexo = 'm';

5--Realice la misma autocombinaci�n que el punto 3 pero agregue la condici�n que las parejas no tengan una diferencia superior a 5 a�os (5 filas)
SELECT f.nombre, f.sexo, f.edad, f.domicilio, m.nombre, m.sexo, m.edad, m.domicilio
FROM clientes f
JOIN clientes m ON f.sexo = 'f' AND m.sexo = 'm' AND ABS(f.edad - m.edad) <= 5;
