Ejercicios propuestos
Un profesor guarda algunos datos de sus alumnos en una tabla llamada "alumnos".

1---Elimine la tabla y cr�ela con la siguiente estructura:


drop table alumnos;

create table alumnos(
    legajo char(5) not null,
    documento char(8) not null,
    nombre varchar2(30),
    curso char(1) not null,
    materia varchar2(20) not null,
    notafinal number(4,2)
);




2--Cree un �ndice no �nico para el campo "nombre".

CREATE INDEX IDX_nombre ON alumnos(nombre);
3--Establezca una restricci�n "primary key" para el campo "legajo"

ALTER TABLE alumnos
ADD CONSTRAINT PK_alumnos_legajo
PRIMARY KEY (legajo);
4--Verifique que se cre� un �ndice con el nombre de la restricci�n.
SELECT index_name, uniqueness
FROM user_indexes
WHERE table_name = 'ALUMNOS';

5---Verifique que se cre� un �ndice �nico con el nombre de la restricci�n consultando el diccionario de �ndices.

SELECT index_name, uniqueness
FROM user_indexes
WHERE table_name = 'ALUMNOS';

6--Intente eliminar el �ndice "PK_alumnos_legajo" con "drop index".


DROP INDEX PK_alumnos_legajo;

7--Cree un �ndice �nico para el campo "documento".
CREATE UNIQUE INDEX IDX_documento ON alumnos(documento);

8--Agregue a la tabla una restricci�n �nica sobre el campo "documento" y verifique que no se cre� un �ndice, Oracle emplea el �ndice creado en el punto anterior.

ALTER TABLE alumnos
ADD CONSTRAINT UQ_alumnos_documento
UNIQUE (documento);

SELECT index_name, uniqueness
FROM user_indexes
WHERE table_name = 'ALUMNOS';
9--Intente eliminar el �ndice "I_alumnos_documento" (no se puede porque una restricci�n lo est� utilizando)


ALTER TABLE alumnos DROP CONSTRAINT nombre_de_restriccion;

10--Elimine la restricci�n �nica establecida sobre "documento".

ALTER TABLE alumnos DROP CONSTRAINT nombre_de_restriccion;

11--Verifique que el �ndice "I_alumnos_documento" a�n existe.
SELECT index_name, table_name
FROM user_indexes
WHERE index_name = 'I_alumnos_documento';


12--Elimine el �ndice "I_alumnos_documento", ahora puede hacerlo porque no hay restricci�n que lo utilice.

DROP INDEX I_alumnos_documento;

13--Elimine el �ndice "I_alumnos_nombre".
DROP INDEX I_alumnos_nombre;


14--Elimine la restricci�n "primary key"/

ALTER TABLE alumnos DROP CONSTRAINT PK_alumnos_legajo;

15--Verifique que el �ndice "PK_alumnos_legajo" fue eliminado (porque fue creado por Oracle al establecerse la restricci�n)

SELECT index_name
FROM user_indexes
WHERE index_name = 'PK_ALUMNOS_LEGAJO';

16--Cree un �ndice compuesto por los campos "curso" y "materia", no �nico.

CREATE INDEX IDX_ALUMNOS_CURSO_MATERIA ON alumnos(curso, materia);

17--Verifique su existencia.
SELECT index_names
FROM user_indexes
WHERE index_name = 'IDX_ALUMNOS_CURSO_MATERIA';


18--Elimine la tabla "alumnos" y verifique que todos los �ndices han sido eliminados junto con ella.
DROP TABLE alumnos;
