--Ejercicios propuestos
Una empresa tiene registrados sus clientes en una tabla llamada "clientes", tambi�n tiene una tabla "provincias" donde registra los nombres de las provincias.

1--Elimine las tablas "clientes" y "provincias":


drop table clientes;
drop table provincias;



2--Cr�elas con las siguientes estructuras:


create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2),
    primary key(codigo)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20),
    primary key (codigo)    );



3---Ingrese algunos registros para ambas tablas:


insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Misiones');
insert into provincias values(4,'Rio Negro');
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1);
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2);
insert into clientes values(102,'Acosta Ana','Avellaneda 333','Posadas',3);




4--Establezca una restricci�n "foreign key" especificando la acci�n "set null" para eliminaciones.

alter table clientes
add constraint fk_clientes_provincias
foreign key (codigoprovincia)
references provincias(codigo)
on delete set null;

5---Elimine el registro con c�digo 3, de "provincias" y consulte "clientes" para ver qu� cambios ha realizado Oracle en los registros coincidentes
--Todos los registros con "codigoprovincia" 3 han sido seteados a null.
delete from provincias where codigo = 3;


select * from clientes;

6--Consulte el diccionario "user_constraints" para ver qu� acci�n se ha establecido para las eliminaciones

select constraint_name, delete_rule
from user_constraints
where table_name = 'CLIENTES' and constraint_type = 'R' and r_constraint_name in (
    select constraint_name
    from user_constraints
    where table_name = 'PROVINCIAS' and constraint_type = 'P'
);

7---Intente modificar el registro con c�digo 2, de "provincias"

update provincias
set nombre = 'Buenos Aires'
where codigo = 2;

8--Elimine la restricci�n "foreign key" establecida sobre "clientes"

alter table clientes
drop constraint nombre_de_la_restriccion;

9--Establezca una restricci�n "foreign key" sobre "codigoprovincia" de "clientes" especificando la acci�n "cascade" para eliminaciones
alter table clientes
add constraint fk_clientes_provincias
foreign key (codigoprovincia)
references provincias (codigo)
on delete cascade;


10--Consulte el diccionario "user_constraints" para ver qu� acci�n se ha establecido para las eliminaciones sobre las restricciones "foreign key" de la tabla "clientes"
select constraint_name, delete_rule
from user_constraints
where table_name = 'CLIENTES' and constraint_type = 'R';


11--Elimine el registro con c�digo 2, de "provincias"

DELETE FROM provincias WHERE codigo = 2;

12--Verifique que el cambio se realiz� en cascada, es decir, que se elimin� en la tabla "provincias" y todos los clientes de la provincia eliminada

SELECT * FROM clientes;

13--Elimine la restricci�n "foreign key"
ALTER TABLE clientes DROP CONSTRAINT nombre_restriccion;


14--Establezca una restricci�n "foreign key" sin especificar acci�n para eliminaciones

ALTER TABLE nombre_tabla
ADD CONSTRAINT nombre_restriccion FOREIGN KEY (columna_referenciada)
REFERENCES tabla_referenciada (columna_referenciada);

15---Intente eliminar un registro de la tabla "provincias" cuyo c�digo exista en "clientes"



DELETE FROM provincias
WHERE codigo = <<c�digo_provincia>>;

16--Consulte el diccionario "user_constraints" para ver qu� acci�n se ha establecido para las eliminaciones sobre la restricci�n "FK_CLIENTES_CODIGOPROVINCIA"

SELECT delete_rule
FROM user_constraints
WHERE constraint_name = 'FK_CLIENTES_CODIGOPROVINCIA';

17--Intente elimimar la tabla "provincias"

DROP TABLE provincias;

18--Elimine la restricci�n "foreign key"

ALTER TABLE provincias DROP CONSTRAINT nombre_restriccion;

19--Elimine la tabla "provincias"

DROP TABLE provincias;

