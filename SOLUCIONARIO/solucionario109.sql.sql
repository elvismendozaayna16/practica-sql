Ejercicios propuestos

Una librer�a almacena los datos de sus libros en una tabla denominada "libros" y controla las acciones que los empleados realizan sobre dicha tabla almacenando en la tabla "control" el nombre del usuario y la fecha, cada vez que se modifica el precio o la editorial de un libro.

01 Elimine las tablas:

drop table control;
drop table libros;




02 Cree las tablas con las siguientes estructuras:

create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);




03 Ingrese algunos registros en "libros":

insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);




04 Establezca el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":

alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';



05 Cree un desencadenador a nivel de sentencia que se dispare cada vez que se actualicen los campos "precio" y "editorial" ; el trigger debe ingresar en la tabla "control", el nombre del usuario, la fecha y la hora en la cual se realiz� un "update" sobre "precio" o "editorial" de "libros"

create or replace trigger modificacion_libro
after update of precio, editorial on libros
declare
  v_usuario varchar2(30);
begin
  select user into v_usuario from dual;
  insert into control(usuario, fecha)
  values(v_usuario, sysdate);
end;
/



06 Vea qu� informa el diccionario "user_triggers" respecto del trigger anteriormente creado

select * from user_triggers where trigger_name = 'MODIFICACION_LIBRO';



07 Aumente en un 10% el precio de todos los libros de editorial "Nuevo siglo'

update libros
set precio = precio * 1.1
where editorial = 'Nuevo siglo';



08 Vea cu�ntas veces se dispar� el trigger consultando la tabla "control" El trigger se dispar� 1 vez.

select * from control;



09 Cambie la editorial, de "Planeta" a "Sudamericana"

update libros
set editorial = 'Sudamericana'
where editorial = 'Planeta';



10 Veamos si el trigger se dispar� consultando la tabla "control" El trigger se dispar�.

select * from control;



11 Modifique un campo diferente de los que activan el trigger

update libros
set titulo = 'Nuevo t�tulo'
where codigo = 100;



12 Verifique que el cambio se realiz�

select * from libros where codigo = 100;



13 Verifique que el trigger no se dispar� El trigger no se dispar� (no hay nuevas filas en "control"), pues est� definido �nicamente sobre los campos "precio" y "editorial".

select * from control;