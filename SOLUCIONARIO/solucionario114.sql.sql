Ejercicios propuestos

Una librer�a almacena los datos de sus libros en una tabla denominada "libros" y controla las acciones que los empleados realizan sobre dicha tabla almacenando en la tabla "control" el nombre del usuario, la fecha, y el tipo de modificaci�n que se realiz� sobre la tabla "libros".

01 Elimine la tabla "libros" y la tabla "control":

drop table libros;
drop table control;




02 Cree las tablas con las siguientes estructuras:

create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date,
    operacion varchar2(20)
);




03 Ingrese algunos registros en "libros":

insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);




04 Cree un disparador que se active cuando modificamos alg�n campo de "libros" y almacene en "control" el nombre del usuario que realiza la actualizaci�n, la fecha y en "operacion" coloque el nombre del campo actualizado

create or replace trigger tr_actualizar_libros
before update on libros
for each row
begin
    insert into control values (user, sysdate, 'actualizacion');
end;
/



05 Cree otro desencadenador que se active cuando ingresamos un nuevo registro en "libros", debe almacenar en "control" el nombre del usuario que realiza el ingreso, la fecha e "insercion" en "operacion"

create or replace trigger tr_ingresar_libros
before insert on libros
for each row
begin
    insert into control values (user, sysdate, 'insercion');
end;
/



06 Cree un tercer trigger sobre "libros" que se active cuando eliminamos un registro de "libros", debe almacenar en "control" el nombre del usuario que realiza la eliminaci�n, la fecha y "borrado" en "operacion"

create or replace trigger tr_eliminar_libros
before delete on libros
for each row
begin
    insert into control values (user, sysdate, 'borrado');
end;
/



07 Los tres triggers est�n habilitados. Consultamos el diccionario "user_triggers" para corroborarlo

select trigger_name, status from user_triggers where table_name = 'LIBROS';



08 Ingrese un libro y compruebe que el trigger "tr_ingresar_libros" se dispara recuperando los registros de "control"

insert into libros values (152,'El anillo del hechicero','Gaskin','Planeta',22);
select * from control;



09 Deshabilite el trigger "tr_ingresar_libros"

alter trigger tr_ingresar_libros disable;



10 Consulte el diccionario "user_triggers" para corroborarlo El trigger "tr_ingresar_libros" est� deshabilitado, "tr_actualizar_libros" y "tr_eliminar_libros" est�n habilitados.

select trigger_name, status from user_triggers where table_name = 'LIBROS';



11 Ingrese un libro y compruebe que el trigger de inserci�n no se dispara recuperando los registros de "control":

insert into libros values(152,'El anillo del hechicero','Gaskin','Planeta',22);
select *from control;




12 Actualice la editorial de varios libros y compruebe que el trigger de actualizaci�n se dispara recuperando los registros de "control"

insert into libros values (155,'El codigo Da Vinci','Dan Brown','Planeta',30);
select * from control;



13 Deshabilite el trigger "tr_actualizar_libros"

alter trigger tr_actualizar_libros disable;



14 Consulte el diccionario "user_triggers" para corroborarlo Los triggers "tr_ingresar_libros" y "tr_actualizar_libros" est�n deshabilitados, "tr_eliminar_libros" est� habilitado.

select trigger_name, status from user_triggers where table_name = 'LIBROS';



15 Borre un libro de "libros" y compruebe que el trigger de borrado se dispar� recuperando los registros de "control"

delete from libros where codigo = 103;
select * from control;




16 Deshabilite el trigger "tr_eliminar_libros"

alter trigger tr_eliminar_libros disable;



17 Consulte el diccionario "user_triggers" para comprobarlo Los tres trigger establecidos sobre "empleados" est�n deshabilitados.

select trigger_name, status from user_triggers where table_name = 'LIBROS';



18 Elimine un libro de "libros" y compruebe que tal registro se elimin� de "libros" pero que el trigger de borrado no se dispara recuperando los registros de "control"

delete from libros where codigo = 120;
select * from control;



19 Habilite el trigger "tr_actualizar_libros"

alter trigger tr_actualizar_libros enable;



20 Actualice el autor de un libro y compruebe que el trigger de actualizaci�n se dispara recuperando los registros de "control"

update libros set autor = 'J.K. Rowling' where codigo = 100;
select * from control;



21 Habilite todos los triggers establecidos sobre "libros"

alter trigger tr_ingresar_libros enable;
alter trigger tr_eliminar_libros enable;
alter trigger tr_actualizar_libros enable;



22 Consulte el diccionario "user_triggers" para comprobar que el estado (status) de todos los triggers establecidos sobre "libros" es habilitado Los tres trigger establecidos sobre "libros" han sido habilitados. Se activar�n ante cualquier sentencia "insert", "update" y "delete".

select trigger_name, status from user_triggers where table_name = 'LIBROS';