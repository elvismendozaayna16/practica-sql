
Trabaje con una tabla llamada "empleados".
1- Elimine la tabla y cr�ela:

 drop table empleados;

 create table empleados(
  documento char(8) not null,
  nombre varchar2(10),
  domicilio varchar2(30),
  ciudad varchar2(20) default 'Buenos Aires'
 );
2- Agregue el campo "legajo" de tipo number(3) y una restricci�n "primary key"
 alter table empleados
  add legajo number(3)
  constraint PK_empleados_legajo primary key;
3- Vea si la estructura cambi� y si se agreg� la restricci�n
describe empleados;
 select *from user_constraints where table_name='EMPLEADOS';

4- Agregue el campo "hijos" de tipo number(2) y en la misma sentencia una restricci�n "check" que no permita valores superiores a 30
 alter table empleados
  add hijos number(2)
  constraint CK_empleados_hijos check (hijos<=30);

5- Ingrese algunos registros:

 insert into empleados values('22222222','Juan Lopez','Colon 123','Cordoba',100,2);
 insert into empleados values('23333333','Ana Garcia','Sucre 435','Cordoba',200,3);
6- Intente agregar el campo "sueldo" de tipo number(6,2) no nulo y una restricci�n "check" que no permita valores negativos para dicho campo.
No lo permite porque no damos un valor por defecto para dicho campo no nulo y los registros existentes necesitan cargar un valor.
alter table empleados
  add sueldo number(6,2) not null
  constraint CK_empleados_sueldo check (sueldo>=0);

7- Agregue el campo "sueldo" de tipo number(6,2) no nulo, con el valor por defecto 0 y una restricci�n "check" que no permita valores negativos para dicho campo
 alter table empleados
  add sueldo number(6,2) default 0 not null
  constraint CK_empleados_sueldo check (sueldo>=0);
8- Recupere los registros

select *from empleados;
9- Vea la nueva estructura de la tabla
 describe empleados;
10- Vea las restricciones
 select *from user_constraints where table_name='EMPLEADOS';
