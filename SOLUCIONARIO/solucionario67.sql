Ejercicios propuesto
Trabaje con una tabla llamada "empleados".

1--Elimine la tabla y cr�ela:


drop table empleados;

create table empleados(
    apellido varchar2(20),
    nombre varchar2(20) not null,
    domicilio varchar2(30)
);




2--Agregue un campo "fechaingreso" de tipo date que acepte valores nulos

alter table empleados add fechaingreso date;

3--Verifique que la estructura de la tabla ha cambiado
desc empleados;


4--Agregue un campo "seccion" de tipo caracter que no permita valores nulos y verifique que el nuevo campo existe

alter table empleados add seccion char(1) not null;

5--Ingrese algunos registros:



insert into empleados values('Lopez','Juan','Colon 123','10/10/1980','Contaduria');
insert into empleados values('Gonzalez','Juana','Avellaneda 222','01/05/1990','Sistemas');
insert into empleados values('Perez','Luis','Caseros 987','12/09/2000','Secretaria');




6--Intente agregar un campo "sueldo" que no admita valores nulos.
alter table empleados add sueldo number(8,2) not null;


7--Agregue el campo "sueldo" no nulo y con el valor 0 por defecto.
alter table empleados add sueldo number(8,2) default 0 not null;


8--Verifique que la estructura de la tabla ha cambiado.
DESC empleados;


