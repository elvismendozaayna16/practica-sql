Ejercicios propuestos
Una empresa tiene registrados sus clientes en una tabla llamada "clientes", tambi�n tiene una tabla "provincias" donde registra los nombres de las provincias.

1--Elimine las tablas "clientes" y "provincias" y cr�elas:


drop table clientes;
drop table provincias;

create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20)
);


En este ejemplo, el campo "codigoprovincia" de "clientes" es una clave for�nea, se emplea para enlazar la tabla "clientes" con "provincias".


2--Intente agregar una restricci�n "foreign key" a la tabla "clientes" que haga referencia al campo "codigo" de "provincias"
--No se puede porque "provincias" no tiene restricci�n "primary key" ni "unique".

ALTER TABLE clientes
ADD CONSTRAINT fk_clientes_codigoprovincia
FOREIGN KEY (codigoprovincia)
REFERENCES provincias(codigo);

3--Establezca una restricci�n "unique" al campo "codigo" de "provincias"
ALTER TABLE provincias
ADD CONSTRAINT uk_provincias_codigo UNIQUE (codigo);


4--Ingrese algunos registros para ambas tablas:



insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Misiones');
insert into provincias values(4,'Rio Negro');
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1);
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2);
insert into clientes values(102,'Acosta Ana','Avellaneda 333','Posadas',3);
insert into clientes values(103,'Luisa Lopez','Juarez 555','La Plata',6);




5--Intente agregar la restricci�n "foreign key" del punto 2 a la tabla "clientes"
--No se puede porque hay un registro en "clientes" cuyo valor de "codigoprovincia" no existe en "provincias".

ALTER TABLE clientes ADD CONSTRAINT fk_clientes_provincias FOREIGN KEY (codigoprovincia) REFERENCES provincias(codigo);

6--Elimine el registro de "clientes" que no cumple con la restricci�n y establezca la restricci�n nuevamente.

DELETE FROM clientes WHERE codigoprovincia NOT IN (SELECT codigo FROM provincias);

ALTER TABLE clientes ADD CONSTRAINT fk_clientes_provincias FOREIGN KEY (codigoprovincia) REFERENCES provincias(codigo);

7--Intente agregar un cliente con un c�digo de provincia inexistente en "provincias"
INSERT INTO clientes (codigo, nombre, domicilio, ciudad, codigoprovincia)
VALUES (104, 'Pedro', 'Calle 123', 'Ciudad X', 5);


8--Intente eliminar el registro con c�digo 3, de "provincias".
--No se puede porque hay registros en "clientes" al cual hace referencia.

DELETE FROM provincias WHERE codigo = 3;

9--Elimine el registro con c�digo "4" de "provincias"
--Se permite porque en "clientes" ning�n registro hace referencia a �l.
DELETE FROM provincias WHERE codigo = 4;


10--Intente modificar el registro con c�digo 1, de "provincias"
--No se puede porque hay registros en "clientes" al cual hace referencia.

UPDATE clientes SET codigoprovincia = 10 WHERE codigoprovincia = 1;
UPDATE provincias SET codigo = 10 WHERE codigo = 1;

11--Vea las restricciones de "clientes" consultando "user_constraints"
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'CLIENTES';


12--Vea las restricciones de "provincias"
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'PROVINCIAS';


13--Intente eliminar la tabla "provincias" (mensaje de error)

DROP TABLE provincias;
14--Elimine la restricci�n "foreign key" de "clientes" y luego elimine la tabla "provincias"

ALTER TABLE clientes
DROP CONSTRAINT nombre_restriccion;

-- Eliminar la tabla 
DROP TABLE provincias;

