--Ejercicios propuestos

Ejercicio 01
---Una empresa tiene registrados datos de sus empleados en una tabla llamada "empleados".

1---Elimine la tabla:


drop table empleados;



2--Cr�ela con la siguiente estructura:


create table empleados (
    documento char(8),
    nombre varchar2(30),
    cantidadhijos number(2),
    seccion varchar2(20),
    sueldo number(6,2) default -1
);



3---Agregue una restricci�n "check" para asegurarse que no se ingresen valores negativos para el sueldo.

Note que el campo "sueldo" tiene establecido un valor por defecto (el valor -1) que va contra la restricci�n; Oracle no controla esto, permite establecer la restricci�n, pero al intentar ingresar un registro con el valor por defecto en tal campo, muestra un mensaje de error.

ALTER TABLE empleados
ADD CONSTRAINT check_sueldo_positive CHECK (sueldo >= 0);

4---Intente ingresar un registro con la palabra clave "default" en el campo "sueldo" (mensaje de error)
insert into empleados (documento, nombre, cantidadhijos, seccion, sueldo)
values ('12345678', 'John Doe', 2, 'Ventas', default);


5---Ingrese algunos registros v�lidos:



insert into empleados values ('22222222','Alberto Lopez',1,'Sistemas',1000);
insert into empleados values ('33333333','Beatriz Garcia',2,'Administracion',3000);
insert into empleados values ('34444444','Carlos Caseres',0,'Contadur�a',6000);




6--Intente agregar otra restricci�n "check" al campo sueldo para asegurar que ninguno supere el valor 5000.
La sentencia no se ejecuta porque hay un sueldo que no cumple la restricci�n.
ALTER TABLE empleados
ADD CONSTRAINT chk_sueldo_max CHECK (sueldo <= 5000);


7---Elimine el registro infractor y vuelva a crear la restricci�n
DELETE FROM empleados WHERE sueldo > 5000;

ALTER TABLE empleados
DROP CONSTRAINT chk_sueldo_max;

ALTER TABLE empleados
ADD CONSTRAINT chk_sueldo_max CHECK (sueldo <= 5000);


8---Establezca una restricci�n "check" para "seccion" que permita solamente los valores "Sistemas", "Administracion" y "Contadur�a".

ALTER TABLE empleados
ADD CONSTRAINT chk_seccion_values CHECK (seccion IN ('Sistemas', 'Administracion', 'Contadur�a'));

9---Ingrese un registro con valor "null" en el campo "seccion".

INSERT INTO empleados (documento, nombre, cantidadhijos, seccion, sueldo)
VALUES ('55555555', 'Juan Perez', 3, NULL, 2000);

10--Establezca una restricci�n "check" para "cantidadhijos" que permita solamente valores entre 0 y 15.

ALTER TABLE empleados
ADD CONSTRAINT CK_empleados_cantidadhijos
CHECK (cantidadhijos >= 0 AND cantidadhijos <= 15);

11--Vea todas las restricciones de la tabla (4 filas)

SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'EMPLEADOS';

12--Intente agregar un registro que vaya contra alguna de las restricciones al campo "sueldo".
Mensaje de error porque se infringe la restricci�n "CK_empleados_sueldo_positivo".

INSERT INTO empleados (documento, nombre, cantidadhijos, seccion, sueldo)
VALUES ('44444444', 'Daniel Torres', 2, 'Ventas', -2000);

13--Intente modificar un registro colocando en "cantidadhijos" el valor "21".

UPDATE empleados
SET cantidadhijos = 21
WHERE documento = '33333333';

14--Intente modificar el valor de alg�n registro en el campo "seccion" cambi�ndolo por uno que no est� incluido en la lista de permitidos.

UPDATE empleados
SET seccion = 'Ventas'
WHERE documento = '22222222';

15--Intente agregar una restricci�n al campo secci�n para aceptar solamente valores que comiencen con la letra "B".
Note que NO se puede establecer esta restricci�n porque va en contra de la establecida anteriormente para el mismo campo, si lo permitiera, no podr�amos ingresar ning�n valor para "seccion".

ALTER TABLE empleados
ADD CONSTRAINT ck_seccion_b
CHECK (seccion LIKE 'B%');


16--Agregue un registro con documento nulo.

INSERT INTO empleados (documento, nombre, cantidadhijos, seccion, sueldo)
VALUES (NULL, 'Juan Perez', 2, 'Sistemas', 2000);

17--Intente agregar una restricci�n "primary key" para el campo "documento".
--No lo permite porque existe un registro con valor nulo en tal campo.

DELETE FROM empleados WHERE documento IS NULL;

18--Elimine el registro que infringe la restricci�n y establezca la restricci�n del punto 17.

DELETE FROM empleados WHERE documento IS NULL;
ALTER TABLE empleados ADD CONSTRAINT PK_empleados_documento PRIMARY KEY (documento);

19--Consulte "user_constraints", mostrando los campos "constraint_name", "constraint_type" y "search_condition" de la tabla "empleados" (5 filas)

SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'EMPLEADOS';

20--Consulte el cat�logo "user_cons_colums" recuperando el nombre de las restricciones establecidas en el campo sueldo de la tabla "empleados" (2 filas)
SELECT constraint_name
FROM user_cons_columns
WHERE table_name = 'EMPLEADOS'
  AND column_name = 'SUELDO';



Ejercicio 02
Una playa de estacionamiento almacena los datos de los veh�culos que ingresan en la tabla llamada "vehiculos".

1--Setee el formato de "date" para que nos muestre d�a, mes, a�o, hora y minutos:


alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';



2--Elimine la tabla "vehiculos" y cr�ela con la siguiente estructura:


drop table vehiculos;

create table vehiculos(
    numero number(5),
    patente char(6),
    tipo char(4),
    fechahoraentrada date,
    fechahorasalida date
);



3---Ingrese algunos registros:


insert into vehiculos values(1,'AIC124','auto','17/01/2017 8:05','17/01/2017 12:30');
insert into vehiculos values(2,'CAA258','auto','17/01/2017 8:10',null);
insert into vehiculos values(3,'DSE367','moto','17/01/2017 8:30','17/01/2017 18:00');



4--Agregue una restricci�n de control que especifique que el campo "tipo" acepte solamente los valores "auto" y "moto":


alter table vehiculos
add constraint CK_vehiculos_tipo_valores
check (tipo in ('auto','moto'));




5---Intente modificar el valor del campo "tipo" ingresando un valor inexistente en la lista de valores permitidos por la restricci�n establecida a dicho campo.
UPDATE vehiculos SET tipo = 'camioneta' WHERE numero = 1;


6--Ingrese un registro con valor nulo para "tipo".
INSERT INTO vehiculos VALUES (4, 'XYZ123', NULL, '17/01/2017 9:00', '17/01/2017 13:00');


7--Agregue una restricci�n de control al campo "fechahoraentrada" que establezca que sus valores no sean posteriores a "fechahorasalida".
ALTER TABLE vehiculos
ADD CONSTRAINT CK_vehiculos_fecha_entrada
CHECK (fechahoraentrada <= fechahorasalida);


8--Intente modificar un registro para que la salida sea anterior a la entrada.

UPDATE vehiculos
SET fechahorasalida = TO_DATE('16/01/2017 10:00', 'DD/MM/YYYY HH24:MI')
WHERE numero = 1;

9--Vea todas las restricciones para la tabla "vehiculos".

SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'VEHICULOS';

10--Ingrese un registro con valor nulo para "fechahoraentrada".
INSERT INTO vehiculos(numero, patente, tipo, fechahoraentrada, fechahorasalida)
VALUES (4, 'XYZ123', 'auto', NULL, TO_DATE('17/01/2017 10:00', 'DD/MM/YYYY HH24:MI'));


11---Vea todos los registros.

SELECT * FROM vehiculos;

12--Consulte "user_cons_columns" y analice la informaci�n retornada.
SELECT constraint_name, column_name
FROM user_cons_columns
WHERE table_name = 'VEHICULOS';
