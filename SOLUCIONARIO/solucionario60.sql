--Ejercicios propuestos
Una empresa tiene registrados sus clientes en una tabla llamada "clientes", tambi�n tiene una tabla "provincias" donde registra los nombres de las provincias.

1----Elimine las tablas "clientes" y "provincias":


drop table clientes;
drop table provincias;



2---Cr�elas con las siguientes estructuras:


create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2),
    primary key(codigo)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20),
    primary key (codigo)
);



3--Ingrese algunos registros para ambas tablas:


insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Misiones');
insert into provincias values(4,'Rio Negro');
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1);
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2);
insert into clientes values(102,'Garcia Juan','Sucre 345','Cordoba',1);
insert into clientes values(103,'Lopez Susana','Caseros 998','Posadas',3);
insert into clientes values(104,'Marcelo Moreno','Peru 876','Viedma',4);
insert into clientes values(105,'Lopez Sergio','Avellaneda 333','La Plata',5);




4--Intente agregar una restricci�n "foreign key" para que los c�digos de provincia de "clientes" existan en "provincias" sin especificar la opci�n de comprobaci�n de datos
---No se puede porque al no especificar opci�n para la comprobaci�n de datos, por defecto es "validate" y hay un registro que no cumple con la restricci�n.
    foreign key(codigoprovincia) references provincias(codigo) enable novalidate

5--Agregue la restricci�n anterior pero deshabilitando la comprobaci�n de datos existentes

alter table clientes add constraint fk_codigoprovincia foreign key (codigoprovincia) references provincias(codigo) disable;

6--Vea las restricciones de "clientes"
select constraint_name, constraint_type
from user_constraints
where table_name = 'CLIENTES';


7---Deshabilite la restricci�n "foreign key" de "clientes"
alter table clientes disable constraint nombre_restriccion;


8---Vea las restricciones de "clientes"

select constraint_name, constraint_type
from user_constraints
where table_name = 'CLIENTES';

9--Agregue un registro que no cumpla la restricci�n "foreign key"
--Se permite porque la restricci�n est� deshabilitada.
alter table clientes enable constraint fk_clientes_provincias;


10---Modifique el c�digo de provincia del cliente c�digo 104 por 9
--Oracle lo permite porque la restricci�n est� deshabilitada.
UPDATE clientes SET codigoprovincia = 9 WHERE codigo = 104;


11--Habilite la restricci�n "foreign key"
ALTER TABLE clientes ENABLE CONSTRAINT nombre_restriccion;


12--Intente modificar un c�digo de provincia existente por uno inexistente.

ALTER TABLE clientes ENABLE CONSTRAINT fk_clientes_provincias;

13--Intentealterar la restricci�n "foreign key" para que valide los datos existentes

ALTER TABLE clientes ENABLE VALIDATE CONSTRAINT fk_clientes_provincias;

14--Elimine los registros que no cumplen la restricci�n y modifique la restricci�n a "enable" y "validate"
DELETE FROM clientes WHERE codigoprovincia NOT IN (SELECT codigo FROM provincias);


ALTER TABLE clientes
ENABLE VALIDATE CONSTRAINT fk_clientes_provincias;


15---Obtenga informaci�n sobre la restricci�n "foreign key" de "clientes"

SELECT constraint_name, constraint_type, table_name, search_condition
FROM user_constraints
WHERE table_name = 'CLIENTES' AND constraint_type = 'R';


