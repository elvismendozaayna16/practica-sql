--Ejercicios propuestos

Ejercicio 01
Una empresa tiene registrados datos de sus empleados en una tabla llamada "empleados".

1--Elimine la tabla:


drop table empleados;



2--Cr�ela con la siguiente estructura:


create table empleados (
    documento char(8),
    nombre varchar2(30),
    seccion varchar2(20)
);




3--Ingrese algunos registros, dos de ellos con el mismo n�mero de documento:


INSERT INTO empleados (documento, nombre, seccion)
VALUES ('12345678', 'Juan P�rez', 'Ventas');

INSERT INTO empleados (documento, nombre, seccion)
VALUES ('23456789', 'Ana G�mez', 'Recursos Humanos');

INSERT INTO empleados (documento, nombre, seccion)
VALUES ('34567890', 'Pedro Rodr�guez', 'Contabilidad');

INSERT INTO empleados (documento, nombre, seccion)
VALUES ('23456789', 'Mar�a L�pez', 'Administraci�n');

INSERT INTO empleados (documento, nombre, seccion)
VALUES ('45678901', 'Luisa Garc�a', 'Ventas');



4--Intente establecer una restricci�n "primary key" para la tabla para que el documento no se repita ni admita valores nulos.
--No lo permite porque la tabla contiene datos que no cumplen con la restricci�n, debemos eliminar (o modificar) el registro que tiene documento duplicado.

DELETE FROM empleados WHERE documento = '23456789';

5--Establecezca la restricci�n "primary key" del punto 4

ALTER TABLE empleados ADD CONSTRAINT pk_empleados PRIMARY KEY (documento);

6--Intente actualizar un documento para que se repita.
--No lo permite porque va contra la restricci�n.
UPDATE empleados SET documento = '12345678' WHERE nombre = 'Mar�a L�pez';


7--Intente establecer otra restricci�n "primary key" con el campo "nombre".
ALTER TABLE empleados ADD CONSTRAINT pk_empleados_nombre PRIMARY KEY (nombre);


8--Vea las restricciones de la tabla "empleados" consultando el cat�logo "user_constraints" (1 restricci�n "P")

SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'EMPLEADOS';

9--Consulte el cat�logo "user_cons_columns"
SELECT constraint_name, column_name
FROM user_cons_columns
WHERE table_name = 'EMPLEADOS';



Ejercicio 02
Una empresa de remises tiene registrada la informaci�n de sus veh�culos en una tabla llamada "remis".

1--Elimine la tabla:


drop table remis;



2--Cree la tabla con la siguiente estructura:


create table remis(
    numero number(5),
    patente char(6),
    marca varchar2(15),
    modelo char(4)
);




3--Ingrese algunos registros sin repetir patente y repitiendo n�mero.

INSERT INTO remis (numero, patente, marca, modelo) VALUES (1001, 'ABC123', 'Ford', '2021');
INSERT INTO remis (numero, patente, marca, modelo) VALUES (1002, 'DEF456', 'Toyota', '2022');
INSERT INTO remis (numero, patente, marca, modelo) VALUES (1003, 'GHI789', 'Chevrolet', '2020');
INSERT INTO remis (numero, patente, marca, modelo) VALUES (1004, 'JKL012', 'Honda', '2019');


4--Ingrese un registro con patente nula.
INSERT INTO remis (numero, patente, marca, modelo) VALUES (1005, NULL, 'Nissan', '2023');


5--Intente definir una restricci�n "primary key" para el campo "numero".

ALTER TABLE remis ADD CONSTRAINT pk_numero PRIMARY KEY (numero);

6--Intente establecer una restricci�n "primary key" para el campo "patente".

ALTER TABLE remis ADD CONSTRAINT pk_patente PRIMARY KEY (patente);

7--Modifique el valor "null" de la patente.
UPDATE remis SET patente = 'MNO345' WHERE numero = 1005;


8--Establezca una restricci�n "primary key" del punto 6.

ALTER TABLE remis ADD CONSTRAINT pk_patente PRIMARY KEY (patente);

9---Vea la informaci�n de las restricciones consultando "user_constraints" (1 restricci�n "P")
SELECT constraint_name, constraint_type, table_name
FROM user_constraints
WHERE table_name = 'REMIS';


10--Consulte el cat�logo "user_cons_columns" y analice la informaci�n retornada (1 restricci�n)

SELECT constraint_name, column_name
FROM user_cons_columns
WHERE table_name = 'REMIS';
