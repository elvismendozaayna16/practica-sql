# 12. Clave primaria (primary key)

## Ejercicios propuestos

## Ejercicio 01

Trabaje con la tabla "libros" de una librería.

1. Elimine la tabla:

```sql
drop table libros;
```
SALIDA
```sh
```
2. Créela con los siguientes campos, estableciendo como clave primaria el campo "codigo":

```sql
create table libros(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    autor varchar2(20),
    editorial varchar2(15),
    primary key (codigo)
);
```
SALIDA
```sh
```
3. Ingrese los siguientes registros:

```sql
insert into libros (codigo,titulo,autor,editorial) values (1,'El aleph','Borges','Emece');
insert into libros (codigo,titulo,autor,editorial) values (2,'Martin Fierro','Jose Hernandez','Planeta');
insert into libros (codigo,titulo,autor,editorial) values (3,'Aprenda PHP','Mario Molina','Nuevo Siglo');
```
SALIDA
```sh
```
4. Ingrese un registro con código repetido (aparece un mensaje de error)
```sql

```
SALIDA
```sh
```

5. Intente ingresar el valor "null" en el campo "codigo"
```sql

```
SALIDA
```sh
```

6. Intente actualizar el código del libro "Martin Fierro" a "1" (mensaje de error)
```sql

```
SALIDA
```sh
```
7. Actualice el código del libro "Martin Fierro" a "10"
```sql

```
SALIDA
```sh
```
8. Vea qué campo de la tabla "LIBROS" fue establecido como clave primaria
```sql

```
SALIDA
```sh
```
9. Vea qué campo de la tabla "libros" (en minúsculas) fue establecido como clave primaria

La tabla aparece vacía porque Oracle no encuentra la tabla "libros", ya que almacena los nombres de las tablas con mayúsculas.
```sql

```
SALIDA
```sh
```
## Ejercicio 02

Un instituto de enseñanza almacena los datos de sus estudiantes en una tabla llamada "alumnos".

1. Elimine la tabla "alumnos":

```sql
drop table alumnos;
```

2. Cree la tabla con la siguiente estructura intentando establecer 2 campos como clave primaria, el campo "documento" y "legajo":

```sql
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(codigo),
    primary key(documento)
);
```

Un mensaje indica la tabla solamente puede tener UNA clave primaria.

3. Cree la tabla estableciendo como clave primaria el campo "documento":

```sql
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);
```

4. Verifique que el campo "documento" no admite valores nulos
```sql

```
SALIDA
```sh
```
5. Ingrese los siguientes registros:

```sql
insert into alumnos (legajo,documento,nombre,domicilio) values('A233','22345345','Perez Mariana','Colon 234');
insert into alumnos (legajo,documento,nombre,domicilio) values('A567','23545345','Morales Marcos','Avellaneda 348');
```

6. Intente ingresar un alumno con número de documento existente (no lo permite)
```sql

```
SALIDA
```sh
```
7. Intente ingresar un alumno con documento nulo (no lo permite)
```sql

```
SALIDA
```sh
```
8. Vea el campo clave primaria de "ALUMNOS".
```sql

```
SALIDA
```sh
```