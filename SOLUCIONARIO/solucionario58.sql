Ejercicios propuestos
Una empresa registra los datos de sus clientes en una tabla llamada "clientes". Dicha tabla contiene un campo que hace referencia al cliente que lo recomend� denominado "referenciadopor". Si un cliente no ha sido referenciado por ning�n otro cliente, tal campo almacena "null".

1---Elimine la tabla y cr�ela:


drop table clientes;
 
create table clientes(
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    referenciadopor number(5),
    primary key(codigo)
);



2--Ingresamos algunos registros:


insert into clientes values (50,'Juan Perez','Sucre 123','Cordoba',null);
insert into clientes values(90,'Marta Juarez','Colon 345','Carlos Paz',null);
insert into clientes values(110,'Fabian Torres','San Martin 987','Cordoba',50);
insert into clientes values(125,'Susana Garcia','Colon 122','Carlos Paz',90);
insert into clientes values(140,'Ana Herrero','Colon 890','Carlos Paz',9);




3--Intente agregar una restricci�n "foreign key" para evitar que en el campo "referenciadopor" se ingrese un valor de c�digo de cliente que no exista.
--No se permite porque existe un registro que no cumple con la restricci�n que se intenta establecer.
ALTER TABLE clientes ADD CONSTRAINT fk_referenciadopor FOREIGN KEY (referenciadopor) REFERENCES clientes(codigo);


4--Cambie el valor inv�lido de "referenciadopor" del registro que viola la restricci�n por uno v�lido:



update clientes set referenciadopor=90 where referenciadopor=9;




5--Agregue la restricci�n "foreign key" que intent� agregar en el punto 3

ALTER TABLE clientes
ADD CONSTRAINT fk_referenciadopor
FOREIGN KEY (referenciadopor)
REFERENCES clientes(codigo);

6--Vea la informaci�n referente a las restricciones de la tabla "clientes"
--La tabla tiene 2 restricciones.
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'CLIENTES';


7--Intente agregar un registro que infrinja la restricci�n.
INSERT INTO clientes VALUES (200, 'Laura Mart�nez', 'Avellaneda 456', 'C�rdoba', 300);


8--Intente modificar el c�digo de un cliente que est� referenciado en "referenciadopor"
UPDATE clientes SET codigo = 300 WHERE codigo = 50;


9---Intente eliminar un cliente que sea referenciado por otro en "referenciadopor"
DELETE FROM clientes WHERE codigo = 50;


10--Cambie el valor de c�digo de un cliente que no referenci� a nadie.

UPDATE clientes SET codigo = 160 WHERE codigo = 140;

11--Elimine un cliente que no haya referenciado a otros.
DELETE FROM clientes WHERE codigo = 110;


