Una empresa almacena la informaci�n de sus clientes en una tabla llamada "clientes".

1- Elimine la tabla:

 drop table clientes cascade constraints;
2- Cree la tabla:

 create table clientes(
  nombre varchar2(40),
  documento char(8),
  domicilio varchar2(30),
  ciudad varchar2(30)
 );
3- Ingrese algunos registros:

 insert into clientes values('Juan Perez','22222222','Colon 1123','Cordoba');
 insert into clientes values('Karina Lopez','23333333','San Martin 254','Cordoba');
 insert into clientes values('Luis Garcia','24444444','Caseros 345','Cordoba');
 insert into clientes values('Marcos Gonzalez','25555555','Sucre 458','Santa Fe');
 insert into clientes values('Nora Torres','26666666','Bulnes 567','Santa Fe');
 insert into clientes values('Oscar Luque','27777777','San Martin 786','Santa Fe');
 insert into clientes values('Pedro Perez','28888888','Colon 234','Buenos Aires');
 insert into clientes values('Rosa Rodriguez','29999999','Avellaneda 23','Buenos Aires');
4- Cree o reemplace la vista "vista_clientes" para que recupere el nombre y ciudad de todos los clientes
 create or replace view vista_clientes
 as
  select nombre, ciudad
  from clientes;

5- Cree o reemplace la vista "vista_clientes2" para que recupere el nombre y ciudad de todos los clientes no permita modificaciones.
 create or replace view vista_clientes2
 as
  select nombre, ciudad
  from clientes
  with read only;

6- Consulte ambas vistas
 select *from vista_clientes;
 select *from vista_clientes2;

7- Intente ingresar el siguiente registro mediante la vista que permite s�lo lectura
Oracle no lo permite.

 insert into vista_clientes2 values ('Ana Acosta','Salta');


8- Ingrese el registro anterior en la vista "vista_clientes"
 insert into vista_clientes values ('Ana Acosta','Salta');


9- Intente modificar un registro mediante la vista que permite s�lo lectura
 update vista_clientes2 set ciudad='Salta' where nombre='Juan Perez';


10- Actualice el registro anterior en la vista "vista_clientes"
 update vista_clientes set ciudad='Salta' where nombre='Juan Perez';


11- Intente eliminar un registro mediante la vista "vista_clientes2"
 delete from vista_clientes2 where ciudad='Buenos Aires';


12- Elimine todos los clientes de "Buenos Aires" a trav�s de la vista "vista_clientes"
 delete from vista_clientes2 where ciudad='Buenos Aires';

