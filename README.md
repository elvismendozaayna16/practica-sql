[1. Crear tablas (create table - describe - all_tables - drop table)](solucionario/solucionario_1.md/)

[2. Ingresar registros (insert into- select)](solucionario/solucionario_2.md/)

[3. Tipos de datos](solucionario/solucionario_3.md/)

[4. Recuperar algunos campos (select)](solucionario/solucionario_4.md/)

[5. Recuperar algunos registros (where)](solucionario/solucionario_5.md/)

[6. Operadores relacionales](solucionario/solucionario_6.md/)

[7. Borrar registros (delete)](solucionario/solucionario_7.md/)

[8. Actualizar registros (update)](solucionario/solucionario_8.md/)

[9. Comentarios](solucionario/solucionario_9.md/)

[10. Valores nulos (null)](solucionario/solucionario_10.md/)

[11. Operadores relacionales (is null)](solucionario/solucionario_11.md/)

[12. Clave primaria (primary key)](solucionario/solucionario_12.md/)

[13. Vaciar la tabla (truncate table)](solucionario/solucionario_13.md/)

[14. Tipos de datos alfanuméricos](solucionario/solucionario_14.md/)

[15. Tipos de datos numéricos](solucionario/solucionario_15.md/)

[16. Ingresar algunos campos]

[17. Valores por defecto (default)]

[18. Operadores aritméticos y de concatenación (columnas calculadas)]

[19.  Alias (encabezados de columnas)](solucionario/solucionario19.sql)

[20. Funciones string](solucionario/solucionario20.sql)

[21. Funciones matemáticas](solucionario/solucionario21.sql)

[22. Funciones de fechas y horas](solucionario/solucionario22.sql)

[23. Ordenar registros (order by)](solucionario/solucionario23.sql)

[24. Operadores lógicos (and - or - not)](solucionario/solucionario24.sql)

[25. Otros operadores relacionales (between)](solucionario/solucionario25.sql)

[26. Otros operadores relacionales (in)](solucionario/solucionario26.sql)

[27. Búsqueda de patrones (like - not like)](solucionario/solucionario27.sql)

[28. Contar registros (count)](solucionario/solucionario28.sql)

[29. Funciones de grupo (count - max - min - sum - avg)](solucionario/solucionario29.sql)

[30. Agrupar registros (group by)](solucionario/solucionario30.sql)

[31. Seleccionar grupos (Having)](solucionario/solucionario31.sql)

[32. Registros duplicados (Distinct)](solucionario/solucionario32.sql)

[33. Clave primaria compuesta](solucionario/solucionario33.sql)

[34. Secuencias (create sequence - currval - nextval - drop sequence)](solucionario/solucionario34.sql)

[35. Alterar secuencia (alter sequence)](solucionario/solucionario35.sql)

[36. Integridad de datos](solucionario/solucionario36.sql)

[37. Restricción primary key](solucionario/solucionario377.sql)

[38. Restricción unique](solucionario/solucionario38.sql)

[39. Restriccioncheck](solucionario/solucionario39.sql)

[40. Restricciones: validación y estados (validate - novalidate - enable - disable)](solucionario/solucionario40.sql)

[41. Restricciones: información (user_constraints - user_cons_columns)](solucionario/solucionario41.sql)

[42. Restricciones: eliminación (alter table - drop constraint)](solucionario/solucionario42.sql)

[43. Indices](solucionario/solucionario43.sql)

[44. Indices (Crear . Información)](solucionario/solucionario44.sql)

[45. Indices (eliminar)](solucionario/solucionario45.sql)

[46. Varias tablas (join)](solucionario/solucionario46.sql)

[47. Combinación interna (join)](solucionario/solucionario47.sql)

[48. Combinación externa izquierda (left join)](solucionario/solucionario48.sql)

[49. Combinación externa derecha (right join)](solucionario/solucionario49.sql)

[50. Combinación externa completa (full join)](solucionario/solucionario50.sql)

[51. Combinaciones cruzadas (cross)](solucionario/solucionario51.sql)

[52. Autocombinación](solucionario/solucionario52.sql)

[53. Combinaciones y funciones de agrupamiento](solucionario/solucionario53.sql)

[54. Combinar más de 2 tablas](solucionario/solucionario54.sql)

[55. Otros tipos de combinaciones](solucionario/solucionario55.sql)

[56. Clave foránea](solucionario/solucionario56.sql)

[57. Restricciones (foreign key)](solucionario/solucionario57.sql)

[58. Restricciones foreign key en la misma tabla](solucionario/solucionario58.sql)

[59. Restricciones foreign key (eliminación)](solucionario/solucionario59.sql)

[60. Restricciones foreign key deshabilitar y validar](solucionario/solucionario60.sql)

[61. Restricciones foreign key (acciones)](solucionario/solucionario61.sql)

[62. Información de user_constraints](solucionario/solucionario62.sql)

[63. Restricciones al crear la tabla](solucionario/solucionario63.sql)

[64. Unión](solucionario/solucionario64.sql)

[65. Intersección](solucionario/solucionario65.sql)

[66. Minus](solucionario/solucionario66.sql)

[67. Agregar campos (alter table-add)](solucionario/solucionario67.sql)

[68. Modificar campos (alter table - modify)](solucionario/solucionario68.sql)

[69. Eliminar campos (alter table - drop)](solucionario/solucionario69.sql)

[70.  Agregar campos y restricciones (alter table)](solucionario/solucionario70.sql)

[71. Subconsultas](solucionario/solucionario71.sql)

[72. Subconsultas como expresion](solucionario/solucionario72.sql)

[73. Subconsultas con in](solucionario/solucionario73.sql)

[74. Subconsultas any- some - all](solucionario/solucionario74.sql)

[75. Subconsultas correlacionadas](solucionario/solucionario75.sql)

[76. Exists y No Exists](solucionario/solucionario76.sql)

[77. Subconsulta simil autocombinacion](solucionario/solucionario77.sql)

[78. Subconsulta conupdate y delete](solucionario/solucionari78.sql)

[79. Subconsulta e insert](solucionario/solucionario79.sql)

[80. Crear tabla a partir de otra (create table-select)](solucionario/solucionario80.sql)

[81. Vistas (create view)](solucionario/solucionario81.sql)

[82. Vistas (información)](solucionario/solucionario82.sql)

[83. Vistas eliminar (drop view)](solucionario/solucionario83.sql)

[84. Vistas (modificar datos a través de ella)](solucionario/solucionario84.sql)

[85. Vistas (with read only)](solucionario/solucionario85.sql)

[86. Vistas modificar (create or replace view)](solucionario/solucionario86.sql)

[87. Vistas (with check option)](solucionario/solucionario87.sql)

[88. Vistas (otras consideraciones: force)](solucionario/solucionario88.sql)

[89. Vistas materializadas (materialized view)](solucionario/solucionario89.sql)

[90. Procedimientos almacenados](solucionario/solucionario90.sql)

[91. Procedimientos Almacenados (crear- ejecutar)](solucionario/solucionario91.sql)

[92. Procedimientos Almacenados (eliminar)](solucionario/solucionario92.sql)

[93. Procedimientos almacenados (parámetros de entrada)](solucionario/solucionario93.sql)

[94. Procedimientos almacenados (variables)](solucionario/solucionario94.sql)

[95. Procedimientos Almacenados (informacion)](solucionario/solucionario95.sql)

[96. Funciones](solucionario/solucionario96.sql)

[97. Control de flujo (if)](solucionario/solucionario97.sql)

[98. Control de flujo (case)](solucionario/solucionario98.sql)

[99. Control de flujo (loop)](solucionario/solucionario99.sql)

[100. Control de flujo (for)](solucionario/solucionario100.sql)

